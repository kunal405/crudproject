package com.java.crudProject.repository;

import com.java.crudProject.model.UserDetails;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface UserDetailsRepository extends CrudRepository<UserDetails, Long> {

}