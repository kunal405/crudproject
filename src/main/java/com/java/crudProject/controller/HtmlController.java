package com.java.crudProject.controller;

import com.java.crudProject.service.AreaInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.Map;

@Controller
@RequestMapping("/html")
public class HtmlController {
    @Autowired
AreaInfoService areaInfoService;
    @RequestMapping(value = "/task", method = RequestMethod.GET)
    public ModelAndView taskList(Map<String, Object> model) {

        ModelAndView modelAndView=new ModelAndView();

        modelAndView.addObject("task",areaInfoService.findAll());
        modelAndView.setViewName("test");
        return modelAndView;
    }
}