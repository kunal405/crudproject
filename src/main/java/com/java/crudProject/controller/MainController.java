package com.java.crudProject.controller;



import com.java.crudProject.exception.ResourceNotFoundException;
import com.java.crudProject.model.AreaInfo;
import com.java.crudProject.service.AreaInfoService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;



import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.annotation.RequestMethod;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.util.List;
import java.util.Map;

import org.springframework.http.MediaType;
import org.springframework.web.servlet.ModelAndView;

@RestController
@RequestMapping("/geo")
public class MainController {

    @Autowired
    AreaInfoService areaInfoService;
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @PostMapping("/adddata")
    public AreaInfo adddata(@RequestBody AreaInfo info)
    {

        AreaInfo b = this.areaInfoService.adddata(info);
        return b;
    }

    // update to create an district*/
    @PutMapping(value = "/updating/{id}")
    public AreaInfo updateIt(@RequestBody AreaInfo areaInfo,@PathVariable("id") long id) {
        areaInfoService.updateRecord(areaInfo,id);
        return areaInfo;
    }


    //get all districts
    @GetMapping("/getall")
    public List<AreaInfo> getAll() {

//        List<AreaInfo> al=areaInfoService.findAll();
//        String result="";
//        for(AreaInfo item:al){
//            result =result+ item.getId() + item.getZipcode()+item.getDistrict()+item.getState() ;
//        }
//        return result;
        return areaInfoService.findAll();
    }



    //delete by id
    @RequestMapping(value = "/dely/{id}", method = RequestMethod.GET)
    void deleteById(@PathVariable long id) throws ResourceNotFoundException{
        areaInfoService.deleteById(id);
    }


    @GetMapping(value = "/find/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<AreaInfo> findById(@PathVariable long id) {
        try {
            AreaInfo info = areaInfoService.findById(id);

            return ResponseEntity.ok(info);  // return 200, with json body
        } catch (ResourceNotFoundException ex) {
            logger.error(ex.getMessage());
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null); // return 404, with null body
        }
    }

}


