package com.java.crudProject.controller;



import ch.qos.logback.core.net.SyslogOutputStream;
import com.java.crudProject.model.UserDetails;
import com.java.crudProject.service.UserDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.Map;
@Controller
@RequestMapping("/form")
public class FormController {


    @Autowired
    UserDetailsService userDetailsService;
    @RequestMapping("/user")
    public String Form() {

        return "Form";
    }
@RequestMapping("/addData")
public String addData(@RequestParam("name") String name,@RequestParam("age") int age,@RequestParam("pnumber") int pnumber,@RequestParam("state") String state) {
        UserDetails details=new UserDetails();


        details.setName(name);
        details.setAge(age);
        details.setPnumber(pnumber);
        details.setState(state);

UserDetails b=this.userDetailsService.adddata(details);
    System.out.print(b);
    return "Form";
}


}
