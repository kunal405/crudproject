package com.java.crudProject.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;

@Getter
@Setter

@Entity(name="userdata")
public class UserDetails implements Serializable {
    private static final long serialVersionUID = -2346866669992342L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "name")
    private String name;

    @Column(name = "age")
    private Integer age;

    @Column(name = "pnumber")
    private Integer pnumber;

    @Column(name = "state")
    private String state;



}
