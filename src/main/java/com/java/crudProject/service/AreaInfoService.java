package com.java.crudProject.service;

import com.java.crudProject.exception.ResourceNotFoundException;
import com.java.crudProject.model.AreaInfo;
import com.java.crudProject.repository.AreaInfoRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import javax.transaction.Transactional;
import java.util.List;

@Service
public class AreaInfoService {


    @Autowired
    private AreaInfoRepository areaInfoRepository;

    private boolean existsById(Long id) {
        return areaInfoRepository.existsById(id);
    }

//    public AreaInfo adddata(AreaInfo b)
//    {
//        //list.add(b);
//
//        return areaInfoRepository.save(b);}

    public AreaInfo adddata(AreaInfo info)  {

        AreaInfo result= this.areaInfoRepository.save(info);
        return result;

    }



    public void updateRecord(AreaInfo areaInfo,long id) {

        // areaInfoRepository.deleteById(id);
        areaInfo.setId(id);
        areaInfoRepository.save(areaInfo);
    }




    public List<AreaInfo> findAll(){
        List<AreaInfo> bes = (List<AreaInfo>)areaInfoRepository.findAll();
        return bes;
    }


    public AreaInfo findById(Long id) throws ResourceNotFoundException {
        AreaInfo area = areaInfoRepository.findById(id).orElse(null);
        if (area==null) {
            throw new ResourceNotFoundException("Cannot find area with id: " + id);
        }
        else return area;
    }



    public void deleteById(long id) throws ResourceNotFoundException{
        if (!existsById(id)) {
            throw new ResourceNotFoundException("Cannot find data with id: " + id);
        }
        else {
            areaInfoRepository.deleteById(id);
        }

    }

}