package com.java.crudProject.service;



import com.java.crudProject.model.UserDetails;
import com.java.crudProject.repository.UserDetailsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserDetailsService {

    @Autowired
    UserDetailsRepository userDetailsRepository;

    public UserDetails adddata(UserDetails info)  {

        UserDetails result= this.userDetailsRepository.save(info);
        return result;

    }
}
