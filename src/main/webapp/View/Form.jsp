<%--
  Created by IntelliJ IDEA.
  User: DELL
  Date: 02-05-2021
  Time: 10:02 PM
  To change this template use File | Settings | File Templates.
--%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>FORM</title>
    <script
            src="https://code.jquery.com/jquery-3.6.0.min.js"
            integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4="
            crossorigin="anonymous"
    ></script>
    <script
            src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"
            integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV"
            crossorigin="anonymous"
    ></script>
    <script
            src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.bundle.min.js"
            integrity="sha384-LtrjvnR4Twt/qOuYxE721u19sVFLVSA4hf/rRt6PrZTmiPltdZcI7q7PXQBYTKyf"
            crossorigin="anonymous"
    ></script>
    <link
            rel="stylesheet"
            href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css"
            integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l"
            crossorigin="anonymous"
    />

    <link
            rel="stylesheezt"
            href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"
            integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN"
            crossorigin="anonymous"
    />


</head>
<body>
<div class="header">
    <h1 class="header-title" style="text-align: center;">ADD DETAILS</h1>
</div>
<div class="container">

    <form   action="addData" method="POST">

        <div class="form-group">
            <label>Name:</label>
            <input type="text" class="form-control" name="name" placeholder="Enter name"  required>
        </div>
        <div class="form-group">
            <label>AGE:</label>
            <input type="number" class="form-control" name="age" placeholder="Enter age"  required>
        </div>
        <div class="form-group">
            <label>PHONE NUMBER:</label>
            <input type="number" class="form-control" name="pnumber" placeholder="Enter phone number"  required>
        </div>
        <div class="form-group">
            <label>STATE:</label>
            <input type="text" class="form-control" name="state" placeholder="Enter state"  required>
        </div>


    <input type="submit" class="btn btn-primary b2" />
    </form>

    <button type="button" class="btn btn-primary b2" onclick="getDetails()">SHOW DETAILS</button>

</div>
</body>
</html>
